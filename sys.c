/*
 * sys.c - Syscalls implementation
 */
#include <devices.h>
#include <utils.h>
#include <io.h>
#include <mm.h>
#include <mm_address.h>
#include <sched.h>
#include <libc.h>
#include <errno.h>
#include <system.h>
#include <header.h>
#include <cbuffer.h>

#define LECTURA 0
#define ESCRIPTURA 1
#define BUFFER_SIZE 256

int global_pid = 2;
extern int nuka_cola_quantum;

int check_fd(int fd, int permissions)
{
  if ((fd != 0) && (fd != 1)) return -EBADFD;
  if (((fd == 0) && (permissions != LECTURA)) || ((fd == 1) && (permissions!=ESCRIPTURA))) return -EBADF;
  return 0;
}

int sys_ni_syscall()
{
	return -ENOSYS; /*ENOSYS*/
}

int sys_getpid()
{
	return current()->PID;
}

int ret_from_fork()
{
  return 0;
}


int sys_fork()
{
  int i;

  struct list_head *lhchild;
  union task_union *tuchild;
  struct task_struct *pare = current();
  if (list_empty(&freequeue)) return -ENOMEM; //No free task
  else lhchild = list_first(&freequeue); //We look for for the first free task

  list_del(lhchild); //Delete result task from freequeue

  tuchild= (union task_union*)list_head_to_task_struct(lhchild); //We add a kernel stack to list head to form a task union
  copy_data(pare, tuchild, sizeof(union task_union)); //We copy father task union to child

  tuchild->task.dir_pages_baseAddr = allocate_DIR();

  //We gonna assign new pages Data+Stack to child process
  int new; //New page
  page_table_entry *child_PT = get_PT(&tuchild->task);
  int pag; //Number of pages founded
  for (pag=0; pag < NUM_PAG_DATA; pag++) {
    new=alloc_frame();
    if (new!=-1) //We've found a free page
    {
      set_ss_pag(child_PT, PAG_LOG_INIT_DATA+pag, new);
    }
    else //We've NOT found a new page -> Deallocate all previous pages
    {

      for (i=0; i<pag; i++)
      {
        free_frame(get_frame(child_PT, PAG_LOG_INIT_DATA + i));
        del_ss_pag(child_PT, PAG_LOG_INIT_DATA + i);
      }

      list_add_tail(&lhchild, &freequeue);//Return lhchild to freequeue

      return -EAGAIN; //Error
    }
  }


  page_table_entry *parent_PT = get_PT(pare); //Obtain father page table entry

  for (pag = 0; pag < NUM_PAG_KERNEL; pag++) {
    set_ss_pag(child_PT, pag, get_frame(parent_PT, pag)); //Assign to child the 'pag' page from kernel father
    }

  for (pag=0; pag<NUM_PAG_CODE; pag++) {
    set_ss_pag(child_PT, PAG_LOG_INIT_CODE + pag, get_frame(parent_PT, PAG_LOG_INIT_CODE+pag)); //Assign to child the code father
    }

  for (pag = NUM_PAG_KERNEL + NUM_PAG_CODE; pag < (NUM_PAG_KERNEL+NUM_PAG_CODE+NUM_PAG_DATA); pag++) {
    int pgparent, pgchild;
    pgparent = (pag<<12);
    pgchild = (pag+NUM_PAG_DATA)<<12;
    set_ss_pag(parent_PT, pag+NUM_PAG_DATA, get_frame(child_PT, pag)); //Assign to father the page of child in order to father has to be able to acces this page
    copy_data((void*)pgparent, (void*)pgchild, PAGE_SIZE); //Copy data from parent domain to child domain
    del_ss_pag(parent_PT, pag+NUM_PAG_DATA); //Deallocate child's pages from parent's page table entry
    }
    //COPIEM EL HEAP EN EL FILL
    int free_pag = ((pare->program_break)>>12)+1;
    for (pag=HEAP_START; pag < ((pare->program_break)>>12) || (pag == ((pare->program_break)>>12) &&  (pare->program_break)%PAGE_SIZE != 0);pag++) {
    while(parent_PT[free_pag].entry != 0 && free_pag<TOTAL_PAGES) free_pag++;
    if (free_pag == TOTAL_PAGES) {
      if (pag != 0) {
        free_pag = ((pare->program_break)>>12)+1;
        --pag;
        set_cr3(get_DIR(pare));
      }
      else return -EINVAL;
    }
    else {

      int new_ph_pag=alloc_frame();
      if (new_ph_pag == -1)	{
        pag--;
        while(pag >= HEAP_START) free_frame(child_PT[pag--].bits.pbase_addr);
        return -EINVAL;
      }
      set_ss_pag(child_PT,pag,new_ph_pag);
      set_ss_pag(parent_PT,free_pag,child_PT[pag].bits.pbase_addr);
      copy_data((void *)((pag)<<12),	(void *)(free_pag<<12), PAGE_SIZE);
      del_ss_pag(parent_PT, free_pag);
      free_pag++;
    }
  }
  set_cr3(get_DIR(pare)); //Flush TLB

  tuchild->task.PID = global_pid;
  ++global_pid;
  //tuchild->task.state=ST_READY;

  tuchild->stack[KERNEL_STACK_SIZE-18] = &ret_from_fork; //14 registers restore all + 4 registers hardware context
  tuchild->stack[KERNEL_STACK_SIZE-19] = 0; // 14+4=18
  tuchild->task.kernel_esp = &tuchild->stack[KERNEL_STACK_SIZE-19];




  //tuchild->task.state=ST_READY;
  list_add_tail(&(tuchild->task.list), &readyqueue);
  set_quantum(tuchild, pare->juanjum);
  //Lets to init stat
  init_stats(&(tuchild->task));
  tuchild->task.sem_destroyed = 0;
  tuchild->task.read_demand = 0;
  tuchild->task.read_demand = 0;
  tuchild->task.buffer_read = 0;
  return tuchild->task.PID;
}


int sys_write(int fd, char * buffer, int size)
{
    int total_bytes;
    total_bytes  = check_fd(fd, ESCRIPTURA);
    if(total_bytes < 0) return total_bytes;
    if ( buffer == NULL)  return -EFAULT;
    if (size < 0) return -EINVAL;
    char buffer_kernel[BUFFER_SIZE];
    total_bytes = 0;
    while (size > 0){
      if (size >= BUFFER_SIZE) {
        if (access_ok(VERIFY_READ, buffer, BUFFER_SIZE) == 0) return -EFAULT;
        if(copy_from_user(buffer, buffer_kernel, BUFFER_SIZE) == -1) return -EIO;
      }
      else {
      if (access_ok(VERIFY_READ, buffer, size) == 0) return -EFAULT;
      if(copy_from_user(buffer, buffer_kernel, size) == -1) return -EIO;
    }
    int nbytes;
    nbytes = BUFFER_SIZE;
    if (size < BUFFER_SIZE) nbytes = size;
    else buffer = buffer + BUFFER_SIZE;
    int  num;
    num = sys_write_console(buffer_kernel, nbytes);
    if ( num < 0) return -EFAULT;
    else total_bytes = total_bytes + num;
    size = size - nbytes;
  }
    return total_bytes;
}

int sys_gettime()
{
  return zeos_ticks;
}

void sys_exit()
{
  int i;
  page_table_entry * pt_current = get_PT(current());
  for(int i = 0; i < N_SEMAPHORE; ++i) if(DGT[i].owner == current()->PID) sys_sem_destroy(DGT[i].id);
  current()->PID=-1;
  int pos = ((int)current()->dir_pages_baseAddr) - ((int)(page_table_entry *)&dir_pages[0]);
  pos = pos / sizeof(dir_pages[0]);
  --dir_references[pos];
  if(dir_references[pos] <= 0){
    int pag;
    for (pag=PAG_LOG_INIT_DATA;pag<TOTAL_PAGES ;pag++){
      free_frame(pt_current[pag].bits.pbase_addr);
    }
    current()->program_break = HEAP_START;
  }
  update_process_state_rr(current(), &freequeue);
  sched_next_rr();
}



int sys_get_stats(int pid, struct stats *st)
{
  if(pid < 0) return -EINVAL;
  if(st == NULL) return -EFAULT;
  if (!access_ok(VERIFY_WRITE, st, sizeof(struct stats))) return -EINVAL;
  int i;
  for(i = 0; i < NR_TASKS; ++i){
    if(task[i].task.PID == pid){
      task[i].task.estadisticas.remaining_ticks = global_quantum;
      if (copy_to_user(&(task[i].task.estadisticas), st, sizeof(struct stats)) == -1) return -EFAULT;
      return 0;
    }
  }
  return -EINVAL;
}


int sys_clone(void (*function)(void), void *stack)
{
  if (!access_ok(VERIFY_READ, function, sizeof(int))) return -EINVAL;
  if (!access_ok(VERIFY_WRITE, stack, sizeof(int))) return -EINVAL;
  struct list_head *l;
  struct task_struct *t;
  if (list_empty(&freequeue)) return -ENOMEM; //No free task
  else l = list_first(&freequeue); //We look for for the first free task
  t = list_head_to_task_struct(l);
  union task_union *tu = (union task_union *)t;
  union task_union *tc = (union task_union *)current();
  list_del(l);
  copy_data(tc, tu,sizeof(union task_union));

  tu->stack[KERNEL_STACK_SIZE-18] = &ret_from_fork;
  tu->stack[KERNEL_STACK_SIZE-19] = 0;
  tu->task.kernel_esp = &tu->stack[KERNEL_STACK_SIZE-19];

  tu->stack[KERNEL_STACK_SIZE-5] = function; //We put at kernel stack the direction of user return
  tu->stack[KERNEL_STACK_SIZE-2] = stack;    // Stack user at kernel stack

  int pos = ((int)current()->dir_pages_baseAddr) - ((int)(page_table_entry *)&dir_pages[0]);
  pos = pos / sizeof(dir_pages[0]);
  ++dir_references[pos];

  tu->task.PID = global_pid;
  ++global_pid;

  //Lets to init stat
  init_stats(&(tu->task));

  //tuchild->task.state=ST_READY;
  list_add_tail(&tu->task.list, &readyqueue);
  set_quantum(tu, current()->juanjum);
  tu->task.sem_destroyed = 0;
  return tu->task.PID;
}

int sys_sem_init(int n_sem, unsigned int value)
{
  if(n_sem < 0) return -EINVAL;
  if(n_sem > 20) return -EINVAL;
  int pos = init_pos_sem(n_sem);
  if(pos < 0) return pos;
  DGT[pos].counter = value;
  DGT[pos].id = n_sem;
  DGT[pos].owner = current()->PID;
  INIT_LIST_HEAD(&DGT[pos].waiting);
  return 0;
}

int sys_sem_wait(int n_sem)
{
  int pos = pos_sem(n_sem);
  if(pos < 0) return pos;
  if (DGT[pos].counter <= 0){
    list_add_tail(&(current()->list), &DGT[pos].waiting);
    sched_next_rr();
  }
  else --DGT[pos].counter;
  if (current()->sem_destroyed){
    current()->sem_destroyed = 0;
    return -1;
  }
  return 0;
}


int sys_sem_signal(int n_sem)
{
  int pos = pos_sem(n_sem);
  if(pos < 0) return pos;
  if(list_empty(&DGT[pos].waiting)) ++DGT[pos].counter;
  else{
    struct list_head *l = list_first(&DGT[pos].waiting);
    struct task_struct *t = list_head_to_task_struct(l);
    list_del(l);
    list_add_tail(&(t->list), &readyqueue);
   }
  return 0;
}

int sys_sem_destroy(int n_sem)
{
   int pos = pos_sem(n_sem);
   if(pos < 0) return pos;
   if(DGT[pos].owner != current()->PID) return -EPERM;
   while(!list_empty(&DGT[pos].waiting)){
     struct list_head *l = list_first(&DGT[pos].waiting);
     struct task_struct *t = list_head_to_task_struct(l);
     t->sem_destroyed = 1;
     list_del(l);
     list_add_tail(&(t->list), &readyqueue);
   }
   DGT[pos].owner = -1;
   return 0;
}

int sys_read_keyboard(char *buf, int count)
{
  struct task_struct *t = current();
  t->pos_read = 0;
  t->read_demand = count;
  t->buffer_read = buf;
  /*if (c_read >= count && list_empty(&keyboardqueue)){
    read_cbuffer(t);
    t->read_demand = t->read_demand - c_read;
    c_read = 0;
    start = end;
  }
  else {
  list_add_tail (&(current()->list), &keyboardqueue);
  sched_next_rr();
  }
  if (t->read_demand == 0) return count;
  else return -1;
  */
  if (list_empty(&keyboardqueue)){ //No hi ha ningu esperant
    if (c_read >= count){ //Podem satisfer la seva petició
      read_cbuffer(t);
      t->read_demand = t->read_demand - count;
      c_read = c_read - count; //Esto lo debería hacer read_cbuffer
      start = start + count;
      start = start % 256;
    }
    else { //No podem satisfer la seva petició completa: bloquejem
      read_cbuffer(t);
      t->read_demand = t->read_demand - c_read;
      c_read = c_read - c_read;//Esto lo deberia hacer read_cbuffer
      start = end = 0;
      start = start % 256;
    }
  }
  else {// There are people waiting at the queue
    list_add_tail (&(current()->list), &keyboardqueue);
    sched_next_rr();
  }
}


int sys_read(int fd, char *buf, int count)
{
  if (fd < 0) return -EBADF; //Canal no valid
  if (check_fd(fd, LECTURA) != 0) return -EBADF; //Canal no de lectura
  if (!access_ok(VERIFY_WRITE, buf , count)) return -EFAULT; //Buffer correcte
  if (buf == NULL) return -EFAULT;
  if (count < 0) return -EINVAL; //Llegin un numero natural de bytes
  int llegits = sys_read_keyboard(buf, count); //llegir
  if(llegits == -1) return -EFAULT;
  return llegits;
}

void *sys_sbrk(int increment) {
  int i;
	struct task_struct *t = current();
	page_table_entry * pt_current = get_PT(t);
	if (increment > 0) { //Reservem memoria
		int end = ((t->program_break)+increment)>>12; //Pàgina final
		if (end < TOTAL_PAGES) { /* Limit inferior del HEAP */
			for(i = ((t->program_break)>>12);i < end || (i==end && 0!=((t->program_break)+increment)%PAGE_SIZE); ++i) {
				if (pt_current[i].entry == 0) {
					int new_ph_pag=alloc_frame();
					if (new_ph_pag == -1) {
						free_frame(new_ph_pag);
						return (void *)-EINVAL;
					}
					set_ss_pag(pt_current,i,new_ph_pag);//Validem pagina a la pt
				}
			}
		}
		else 	return (void *)-ENOMEM; //Heap esgotat
	}
	else if (increment < 0) { //Lliberem memoria
		page_table_entry * dir_current = get_DIR(t);
		int new_pb = ((t->program_break)+increment)>>12;
		for(i = ((t->program_break)>>12);(i > new_pb || (i==new_pb && 0==((t->program_break)+increment)%PAGE_SIZE)) && i >= HEAP_START; --i) {
			free_frame(pt_current[i].bits.pbase_addr);
			del_ss_pag(pt_current, i);
		}
			set_cr3(dir_current);
	}
  if(t->program_break+increment < HEAP_START<<12) t->program_break = HEAP_START<<12;
	else (t->program_break) += increment;
  return t->program_break;
}
