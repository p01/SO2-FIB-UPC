
#define C_BSIZE 256

extern unsigned char c_buffer[C_BSIZE];

extern int start, end, c_read;

void add_c_cbuffer(unsigned char c);

void read_cbuffer(struct task_struct *t);
