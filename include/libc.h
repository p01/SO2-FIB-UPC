/*
 * libc.h - macros per fer els traps amb diferents arguments
 *          definició de les crides a sistema
 */




#ifndef __LIBC_H__
#define __LIBC_H__

#include <stats.h>
#include <wrapper.h>

int errno;

int gettime();

int check_fd(int fd, int permissions);

void itoa(int a, char *b);

int strlen(char *a);

void perror();

int getpid();

int fork();

void exit();


#endif  /* __LIBC_H__ */
