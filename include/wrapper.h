#include <sched.h>

int write(int fd, char *buffer, int size);

int gettime();

void task_switch (union task_union*t);

void inner_task_switch (union task_union*t);

int getpid(void);

int fork(void);

void exit(void);

int get_stats(int pid, struct stats *st);

int clone (void (*function)(void), void *stack);

int sem_init(int n_sem, unsigned int value);

int sem_wait(int n_sem);

int sem_signal(int n_sem);

int sem_destroy(int n_sem);

int read (int fd, char *buf, int count);

void *sbrk(int increment);
