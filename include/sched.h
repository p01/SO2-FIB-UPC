/*
 * sched.h - Estructures i macros pel tractament de processos
 */

#ifndef __SCHED_H__
#define __SCHED_H__

#include <list.h>
#include <types.h>
#include <mm_address.h>
#include <stats.h>

#define NR_TASKS      10
#define KERNEL_STACK_SIZE	1024
#define QUANTUM 5
#define N_SEMAPHORE 20

enum state_t { ST_RUN, ST_READY, ST_BLOCKED };

int dir_references[NR_TASKS];


struct task_struct {
  int PID;			/* Process ID. This MUST be the first field of the struct. */
  int kernel_esp;
  page_table_entry * dir_pages_baseAddr;
  struct list_head list;
  int state;
  int juanjum;
  int sem_destroyed;
  struct stats estadisticas;
  int program_break;
  int read_demand;
  int pos_read;
  char * buffer_read;
};

union task_union {
  struct task_struct task;
  unsigned long stack[KERNEL_STACK_SIZE];    /* pila de sistema, per procés */
};

struct semaphore {
  int id;
  int owner;
  int counter;
  struct list_head waiting;
};

extern struct semaphore DGT[N_SEMAPHORE];

int global_quantum;
extern union task_union protected_tasks[NR_TASKS+2];
extern union task_union *task; /* Vector de tasques */
extern struct task_struct *idle_task;
extern struct task_struct *task1;
extern struct list_head freequeue;
extern struct list_head readyqueue;
extern struct list_head keyboardqueue;


#define KERNEL_ESP(t)       	(DWord) &(t)->stack[KERNEL_STACK_SIZE]

#define INITIAL_ESP       	KERNEL_ESP(&task[1])

/* Inicialitza les dades del proces inicial */
void stat_sysenter();

int pos_sem(int ident);

int init_pos_sem (int ident);

void stat_go_ready();

void stat_go_running();

void stat_go_block();

void stat_sysexit();

void init_stats(struct task_struct *tt);

void init_task1(void);

void init_idle(void);

void init_sched(void);

void sched();

struct task_struct * current();

void task_switch(union task_union *new);

void inner_task_switch(union task_union *new);

struct task_struct *list_head_to_task_struct(struct list_head *l);

page_table_entry * allocate_DIR();

page_table_entry * get_PT (struct task_struct *t) ;

page_table_entry * get_DIR (struct task_struct *t) ;

int torna_final_pila(union task_union *new);

int fork_or_thread(union task_union *new);

void init_cbuffer();

/* Headers for the scheduling policy */
int get_quantum(struct task_struct *t);
void set_quantum(struct task_struct *t, int new_quantum);
void sched_next_rr();
void update_process_state_rr(struct task_struct *t, struct list_head *dest);
int needs_sched_rr();
void update_sched_data_rr();

#endif  /* __SCHED_H__ */
