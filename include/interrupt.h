/*
 * interrupt.h - Definició de les diferents rutines de tractament d'exepcions
 */

#ifndef __INTERRUPT_H__
#define __INTERRUPT_H__

#include <types.h>

#define IDT_ENTRIES 256

extern Gate idt[IDT_ENTRIES];
extern Register idtR;

void setInterruptHandler(int vector, void (*handler)(), int maxAccessibleFromPL);
void setTrapHandler(int vector, void (*handler)(), int maxAccessibleFromPL);

void syscall_handler_sysenter();
int writeMSR(int msr_addr, int msr_value);

void setIdt();

void keyboard_handler();
void keyboard_routine();


void clock_handler();
void clock_routine();

/*void system_call_handler(); */


#endif  /* __INTERRUPT_H__ */
