/*
 * sched.c - initializes struct for task 0 anda task 1
 */

#include <sched.h>
#include <mm.h>
#include <io.h>
#include <wrapper.h>
#include <errno.h>
#include <cbuffer.h>

/**
 * Container for the Task array and 2 additional pages (the first and the last one)
 * to protect against out of bound accesses.
 */

 struct list_head freequeue;
 struct list_head readyqueue;
 struct list_head keyboardqueue;

union task_union protected_tasks[NR_TASKS+2]
  __attribute__((__section__(".data.task")));
struct task_struct *idle_task;
struct task_struct *task1;
union task_union *task = &protected_tasks[1]; /* == union task_union task[NR_TASKS] */

#if 0
struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  return list_entry( l, struct task_struct, list);
}
#endif

extern struct list_head blocked;
struct semaphore DGT[N_SEMAPHORE];
int dir_references[NR_TASKS];

struct task_struct *list_head_to_task_struct(struct list_head *l)
{
  return list_entry( l, struct task_struct, list);
}

/* get_DIR - Returns the Page Directory address for task 't' */
page_table_entry * get_DIR (struct task_struct *t)
{
	return t->dir_pages_baseAddr;
}

/* get_PT - Returns the Page Table address for task 't' */
page_table_entry * get_PT (struct task_struct *t)
{
	return (page_table_entry *)(((unsigned int)(t->dir_pages_baseAddr->bits.pbase_addr))<<12);
}

void stat_sysenter(){ //user to system
  current()->estadisticas.user_ticks += get_ticks() - current()->estadisticas.elapsed_total_ticks;
  current()->estadisticas.elapsed_total_ticks = get_ticks();
}

void stat_go_ready(){ //block or system to ready
  current()->estadisticas.system_ticks += get_ticks() - current()->estadisticas.elapsed_total_ticks;
  current()->estadisticas.elapsed_total_ticks = get_ticks();
}

void stat_go_running(){ //ready to system
  current()->estadisticas.ready_ticks += get_ticks() - current()->estadisticas.elapsed_total_ticks;
  current()->estadisticas.elapsed_total_ticks = get_ticks();
  ++current()->estadisticas.total_trans;

}

void stat_go_block(){ //system to block
  current()->estadisticas.system_ticks += get_ticks() - current()->estadisticas.elapsed_total_ticks;
  current()->estadisticas.elapsed_total_ticks = get_ticks();
}



void stat_sysexit (){ //system to user
  current()->estadisticas.system_ticks += get_ticks() - current()->estadisticas.elapsed_total_ticks;
  current()->estadisticas.elapsed_total_ticks = get_ticks();


}

int pos_sem(int ident){
  int pos;
  for (pos = 0; pos < N_SEMAPHORE; ++pos) {
    if (DGT[pos].owner != -1 && DGT[pos].id == ident) return pos;
  }
  return -EINVAL;
}

int init_pos_sem (int ident) {
  int pos;
  int pos_init;
  pos_init = -1;
  for (pos  = 0; pos < N_SEMAPHORE; ++pos) {
    if (DGT[pos].owner == -1 && pos_init == -1) pos_init = pos;
    if (DGT[pos].owner != -1 && DGT[pos].id == ident) return -EBUSY;
  }
  if (pos_init == -1) return -EINVAL;
  return pos_init;
}

page_table_entry * allocate_DIR() {
    int i;
    for ( i = 0; i < NR_TASKS; ++i ) {
      if (dir_references[i] == 0){
         ++dir_references[i];
         return &dir_pages[i];
       }
    }
    return -1;
}

void cpu_idle(void)
{

	__asm__ __volatile__("sti": : :"memory");
  while(1);
	{
	;
	}
}

int torna_final_pila(union task_union *new)
{
  return (DWord) &(new)->stack[KERNEL_STACK_SIZE];
}

int fork_or_thread(union task_union *new)
{
  if(current()->dir_pages_baseAddr == new->task.dir_pages_baseAddr) return 0;
  return -1;
}

void init_stats(struct task_struct *tt) {
  tt->estadisticas.user_ticks = 0;
  tt->estadisticas.system_ticks = 0;
  tt->estadisticas.blocked_ticks = 0;
  tt->estadisticas.ready_ticks = 0;
  tt->estadisticas.elapsed_total_ticks = 0;
  tt->estadisticas.total_trans = 0;
  tt->estadisticas.remaining_ticks = 0;
}

void init_idle (void)
{
  struct list_head *l_idle = list_first(&freequeue);
  struct task_struct *t_idle = list_head_to_task_struct(l_idle);
  union task_union * t_union_idle = (union task_union *)t_idle;
  list_del(l_idle);
  t_idle->PID = 0;
  t_idle->dir_pages_baseAddr = allocate_DIR();
  t_union_idle->stack[KERNEL_STACK_SIZE-1] = &cpu_idle;
  t_union_idle->stack[KERNEL_STACK_SIZE-2] = 0;
  t_idle->kernel_esp = &t_union_idle->stack[KERNEL_STACK_SIZE-2];
  idle_task = t_idle;
  set_quantum(idle_task, QUANTUM);
  init_stats(idle_task);
  idle_task->state = ST_READY;
  idle_task->sem_destroyed = 0;
}

void init_task1(void)
{
  struct list_head *l_task1 = list_first(&freequeue);
  struct task_struct *t_task1 = list_head_to_task_struct(l_task1);
  union task_union *t_union_task1 = (union task_union *)t_task1;
  list_del(l_task1);
  t_task1->PID = 1;
  t_task1->dir_pages_baseAddr = allocate_DIR();
  set_user_pages(t_task1);
  tss.esp0 = KERNEL_ESP(t_union_task1);
  writeMSR(0x175, KERNEL_ESP(t_union_task1));
  set_cr3(t_task1->dir_pages_baseAddr);
  task1 = t_task1;
  set_quantum(task1, QUANTUM);
  init_stats(task1);
  task1->state = ST_RUN;
  task1->sem_destroyed = 0;
  task1->program_break = HEAP_START<<12;
  task1->read_demand = 0;
  task1->pos_read = 0;
  task1->buffer_read = 0;
}



void init_sched()
{
  INIT_LIST_HEAD(&freequeue);
  int i;
  for ( i=0; i < NR_TASKS; ++i){
      list_add_tail(&(task[i].task.list), &freequeue);
      dir_references[i] = 0;
  }
  INIT_LIST_HEAD(&readyqueue);
  global_quantum = QUANTUM;
//init Semaphores
  for (i = 0; i < N_SEMAPHORE; ++i){
    DGT[i].owner = -1;
  }
}


struct task_struct* current()
{
  int ret_value;

  __asm__ __volatile__(
  	"movl %%esp, %0"
	: "=g" (ret_value)
  );
  return (struct task_struct*)(ret_value&0xfffff000);
}

int get_quantum(struct task_struct *t) {
  return t->juanjum;
}

void set_quantum(struct task_struct * t, int new_quantum) {
   t->juanjum = new_quantum;
}

void update_sched_data_rr() {
  --global_quantum;
}

int needs_sched_rr() {
  if (global_quantum == 0) return 1;
  return 0;
}

void sched_next_rr() {
  if (current() == idle_task){
    if (list_empty(&readyqueue)) global_quantum = get_quantum(idle_task);
    else {
      struct list_head *c = list_first(&readyqueue);
      struct task_struct *p_next = list_head_to_task_struct(c);
      list_del(c);
      update_process_state_rr (p_next, NULL);
      }
    }
  else {
    if (!list_empty(&readyqueue)){
      struct list_head *d = list_first(&readyqueue);
      struct task_struct *d_next = list_head_to_task_struct(d);
      list_del(d);
      update_process_state_rr(d_next, NULL);
    }
    else {
      update_process_state_rr(idle_task, NULL);
    }
  }
}



void update_process_state_rr(struct task_struct *t, struct list_head *dest) {
  if (dest == NULL) {//Posem en marxa un nou process
    stat_go_ready();
    global_quantum = get_quantum(t);
    task_switch(t);
    stat_sysenter();
  }
  else {
    list_add_tail(&(t->list), dest); //Pasem un process a la seva cua
  }
}


void sched() {
  update_sched_data_rr();
  if (needs_sched_rr()){
    if (current() != idle_task) update_process_state_rr(current(), &readyqueue);
    sched_next_rr();
  }
}

void init_cbuffer() {
  start = end = c_read = 0;
  INIT_LIST_HEAD(&keyboardqueue);
}
