
#include <sched.h>
#include <utils.h>
#include <list.h>
#include <cbuffer.h>


unsigned char c_buffer[C_BSIZE];

int start, end, c_read;

void add_c_cbuffer(unsigned char c)
{
  if(c_read < C_BSIZE){ //No esta buid
    c_buffer[end] = c;//Afegim el caracter
    ++c_read;
    ++end;
    if(end == C_BSIZE) end == 0;
    if (!(list_empty(&keyboardqueue))){ //Mirem la petició a atendre
      struct list_head *l = list_first(&keyboardqueue);
      struct task_struct *t = list_head_to_task_struct(l);
      if(t->read_demand == c_read || c_read == C_BSIZE){
        read_cbuffer(t);
        t->read_demand = t->read_demand - c_read; // Esto lo debería hacer el read
        c_read = 0;
        start = end;
        if(t->read_demand == 0)//Mandar a la ready
          list_del(l);
          update_process_state_rr(t, NULL);
        }
      }
    }
  }

void read_cbuffer(struct task_struct *t) { //No siempre va a leer c_read bytes!!!
  //Debe gestionar ser esta función la que gestiona start, end y c_read.
  int origen = c_buffer[start];
  int origen2 = &(c_buffer[start]);
  int desti = &(t->buffer_read[t->pos_read]);
  if(start < end) {
    copy_to_user(origen2, desti, end-start);
    t->pos_read = t->pos_read + (end-start);
  }
  else if(start == end) {
    if(start == 0){
      copy_to_user(&c_buffer[start], (t->buffer_read[t->pos_read]), C_BSIZE);
      t->pos_read = t->pos_read + C_BSIZE;
    }
    else{
      copy_to_user(&c_buffer[start], (t->buffer_read[t->pos_read]), C_BSIZE-1-start);
      t->pos_read = t->pos_read + (C_BSIZE-1-start);
      copy_to_user(&c_buffer[0], &(t->buffer_read[t->pos_read]), end);
      t->pos_read = t->pos_read + end;
    }
  }
  else{
    copy_to_user(&c_buffer[start], (t->buffer_read[t->pos_read]), C_BSIZE-1-start);
    t->pos_read = t->pos_read + (C_BSIZE-1-start);
    copy_to_user(&c_buffer[0], &(t->buffer_read[t->pos_read]), end);
    t->pos_read = t->pos_read + end;
  }
}
